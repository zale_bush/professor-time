﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
  public void StartSlowTime()
  {
    Time.timeScale = .4f;
  }

  public void StartFastTime()
  {
    Time.timeScale = 1.4f;
  }

  public void StopSlowTime()
  {
    Time.timeScale = 1;
  }

  public void StopFastTime()
  {
    Time.timeScale = 1f;
  }
}

Author: Alessio Regalbuto

Thank you for your download!

With your purchase you acquired an high quality set of VR materials that you can use without limitation in your Unity projects.

If you want to see a demo of the entire set, go inside the folder "Scenes" of this package and open the included scenes.

If you liked the package please leave a good feedback on the store, it is very important for me to keep on working for the community. 
If you have any request or idea to suggest feel free to contact me by mail: alessioregalbuto@outlook.com

Check all my Unity packages here: https://www.assetstore.unity3d.com/en/#!/search/page=1/sortby=popularity/query=publisher:17101
Check my portfolio here: https://alessioregalbuto.com

It would be my pleasure if you mention my name where you will include my VR materials pack :)

Have fun!
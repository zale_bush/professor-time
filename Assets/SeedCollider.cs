﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Seed")
        {
            other.gameObject.GetComponent<SeedListener>().CheckSeed();
        }
    }
}

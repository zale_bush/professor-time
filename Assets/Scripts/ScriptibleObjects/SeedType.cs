﻿using UnityEngine;

[CreateAssetMenu(fileName ="SeedType", menuName = "SeedType" ,order =1)]
public class SeedType : ScriptableObject
{
    public bool GoodSeed;

}

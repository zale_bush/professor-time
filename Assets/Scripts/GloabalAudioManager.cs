﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GloabalAudioManager : MonoBehaviour
{
  List<AudioSource> sourcesPool;

  [System.Serializable]
  public class AudioSnip
  {
    public string name;
    public AudioClip[] clips;
    public bool onlyUseOnce;
    bool used;

    public AudioClip GetRandomClip()
    {
      return clips[(int)Random.Range(0, clips.Length)];
    }
  }

  public enum soundTypes { ambiant, dialoug, effects, character }

  public AudioSnip[] ambiantClips;
  public AudioSnip[] dialougClips;
  public AudioSnip[] effectClips;
  public AudioSnip[] characterClips;

  public GameObject audioSourcePrefab;

  [HideInInspector]
  public static GloabalAudioManager instance;

  private void Start()
  {
    if (GloabalAudioManager.instance == null)
      GloabalAudioManager.instance = this;
    else
      Destroy(gameObject);

    SetupSourcePool();
  }

  public void PlayAudio(Transform sourcePos, string audioName, soundTypes soundType)
  {
    AudioSource tempSource = MoveAvailableSource();
    tempSource.PlayOneShot(FindClip(audioName, soundType));
  }

  AudioSource MoveAvailableSource()
  {
    if (!sourcesPool[0].isPlaying)
    {
      return sourcesPool[0];
    }
    else
    {
      for (int i = 1; i < sourcesPool.Count - 1; i++)
      {
        if (!sourcesPool[i].isPlaying)
          return sourcesPool[i];
      }
    }
    GloabalAudioManager.instance.sourcesPool.Add(Instantiate(audioSourcePrefab).GetComponent<AudioSource>());

    return sourcesPool[sourcesPool.Count - 1];
  }

  AudioClip FindClip(string audioName, soundTypes soundType)
  {
    AudioSnip[] tempAudioArray = new AudioSnip[0];
    switch (soundType)
    {
      case soundTypes.ambiant:
        tempAudioArray = ambiantClips;
        break;
      case soundTypes.character:
        tempAudioArray = characterClips;
        break;
      case soundTypes.dialoug:
        tempAudioArray = dialougClips;
        break;
      case soundTypes.effects:
        tempAudioArray = effectClips;
        break;
    }

    foreach (var sound in tempAudioArray)
    {
      if (sound.name == audioName)
        return sound.GetRandomClip();
    }

    Debug.Log("No clip matching name");
    return null;
  }

  void SetupSourcePool()
  {
    GloabalAudioManager.instance.sourcesPool = new List<AudioSource>();

    for (int i = 0; i < 6; i++)
    {
      GloabalAudioManager.instance.sourcesPool.Add(Instantiate(audioSourcePrefab).GetComponent<AudioSource>());
    }
  }
}

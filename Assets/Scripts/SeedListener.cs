﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedListener : MonoBehaviour
{
    public SeedType seed;
    public Animator anime;

    private void OnMouseDown()
    {
       //CheckSeed();
    }

    public void CheckSeed()
    {
        if (seed.GoodSeed)//Seed is good
            ActiveteSeedPower();

        if (!seed.GoodSeed)//Seed is bad
            ErrorBot();
    }

    public void ActiveteSeedPower()
    {
        anime.gameObject.SetActive(true);
        anime.enabled = true;

    }

    public void ErrorBot()
    { 
        //Nothing Happenes when is a bad Seed
    }
}

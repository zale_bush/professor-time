﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalAudioManager : MonoBehaviour
{
  List<AudioSource> sourcesPool;

  [System.Serializable]
  public class AudioSnip
  {
    public string name;
    public AudioClip[] clips;
    public bool onlyUseOnce;
    bool used;

    [HideInInspector]
    public AudioSource looper;

    public AudioClip GetRandomClip()
    {
      return clips[(int)Random.Range(0, clips.Length)];
    }
  }

  public enum soundTypes { ambiant, dialoug, effects, character }

  public AudioSnip[] ambiantClips;
  public AudioSnip[] dialougClips;
  public AudioSnip[] effectClips;
  public AudioSnip[] characterClips;
  public AudioSnip[] interjectionClips;

  public AudioSource audioSourcePrefab;

  [HideInInspector]
  public static GlobalAudioManager instance;

  private void Start()
  {
    if (GlobalAudioManager.instance == null)
      GlobalAudioManager.instance = this;
    else
      Destroy(gameObject);

    SetupSourcePool();
  }

  public void PlayAudio(string audioName, soundTypes soundType, Transform sourcePos = null)
  {
    AudioSource tempSource = GetAvailableSource();
    if (sourcePos != null)
      tempSource.transform.position = sourcePos.position;
    else
      tempSource.transform.position = GameManager.instance.ambientPos.transform.position;

    tempSource.PlayOneShot(FindClip(audioName, soundType));
  }

  public void StartLooping(string audioName, soundTypes soundType, Transform sourcePos = null)
  {
    AudioSnip Tempsnip = FindSnip(audioName, soundType);

    if (sourcePos != null)
      Tempsnip.looper.transform.position = sourcePos.position;
    else
      Tempsnip.looper.transform.position = GameManager.instance.ambientPos.transform.position;
    Tempsnip.looper = GetAvailableSource();
    Tempsnip.looper.loop = true;
    Tempsnip.looper.clip = Tempsnip.GetRandomClip();
    Tempsnip.looper.Play();
  }

  public void StopLooping(string audioName, soundTypes soundType)
  {
    AudioSnip audioSnip = FindSnip(audioName, soundType);
    audioSnip.looper.Stop();
    audioSnip.looper.clip = null;
    audioSnip.looper.loop = false;
  }

  AudioSource GetAvailableSource()
  {
    if (!sourcesPool[0].isPlaying)
    {
      return sourcesPool[0];
    }
    else
    {
      for (int i = 1; i < sourcesPool.Count - 1; i++)
      {
        if (!sourcesPool[i].isPlaying)
          return sourcesPool[i];
      }
    }
    GlobalAudioManager.instance.sourcesPool.Add(Instantiate(audioSourcePrefab).GetComponent<AudioSource>());

    return sourcesPool[sourcesPool.Count - 1];
  }

  AudioClip FindClip(string audioName, soundTypes soundType)
  {
    AudioSnip[] tempAudioArray = new AudioSnip[0];
    switch (soundType)
    {
      case soundTypes.ambiant:
        tempAudioArray = ambiantClips;
        break;
      case soundTypes.character:
        tempAudioArray = characterClips;
        break;
      case soundTypes.dialoug:
        tempAudioArray = dialougClips;
        break;
      case soundTypes.effects:
        tempAudioArray = effectClips;
        break;
    }

    foreach (var sound in tempAudioArray)
    {
      if (sound.name == audioName)
        return sound.GetRandomClip();
    }

    Debug.Log("No clip matching name");
    return null;
  }

  AudioSnip FindSnip(string audioName, soundTypes soundType)
  {
    AudioSnip[] tempAudioArray = new AudioSnip[0];
    switch (soundType)
    {
      case soundTypes.ambiant:
        tempAudioArray = ambiantClips;
        break;
      case soundTypes.character:
        tempAudioArray = characterClips;
        break;
      case soundTypes.dialoug:
        tempAudioArray = dialougClips;
        break;
      case soundTypes.effects:
        tempAudioArray = effectClips;
        break;
    }

    foreach (var sound in tempAudioArray)
    {
      if (sound.name == audioName)
        return sound;
    }

    Debug.Log("No clip matching name");
    return null;
  }

  AudioSnip[] GetCorrectArray(soundTypes soundType)
  {
    switch (soundType)
    {
      case soundTypes.ambiant:
        return ambiantClips;

      case soundTypes.character:
        return characterClips;

      case soundTypes.dialoug:
        return dialougClips;

      case soundTypes.effects:
        return effectClips;
    }

    return null;
  }

  void SetupSourcePool()
  {
    GlobalAudioManager.instance.sourcesPool = new List<AudioSource>();

    for (int i = 0; i < 6; i++)
    {
      GlobalAudioManager.instance.sourcesPool.Add(Instantiate(audioSourcePrefab).GetComponent<AudioSource>());
    }
  }
}

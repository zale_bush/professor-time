﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GravitySwitch : MonoBehaviour
{
    private float speedmod = 0.2f;
    public Vector3 tempsavevelocity;


    void Start()
    {
        
    }

    // Disables gravity on all rigidbodies entering this collider.
    void OnTriggerEnter(Collider other)
    {
        other.attachedRigidbody.useGravity = false;
         
        tempsavevelocity = other.attachedRigidbody.velocity;
        other.attachedRigidbody.velocity = new Vector3(speedmod*tempsavevelocity.x, speedmod * tempsavevelocity.y, speedmod * tempsavevelocity.z);
        
    }

    void OnTriggerExit(Collider other)
    {
        other.attachedRigidbody.useGravity = true;
        other.attachedRigidbody.velocity = tempsavevelocity;
    }
}
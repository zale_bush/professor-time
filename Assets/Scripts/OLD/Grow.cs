﻿using UnityEngine;

public class Grow : MonoBehaviour
{
    public float delayTimer;

    public float growthFloat = 3;
    public bool y;
    public bool x;
    public bool z;

    public bool fullGrown;

    private void Start()
    {
        if (delayTimer > 0)
        {
            this.GetComponentInChildren<MeshRenderer>().enabled = false;
        }
    }

    

    void Update()
    {
        if (delayTimer <= 0)
        {
            this.GetComponentInChildren<MeshRenderer>().enabled = true;
            if (y && !fullGrown)
            {

                // Set the x position to loop between 0 and 3
                transform.localScale = new Vector3(this.transform.localScale.x, Mathf.PingPong(Time.time, growthFloat), this.transform.localScale.z);
                transform.position = new Vector3(this.transform.position.x, (Mathf.PingPong(Time.time, growthFloat) / 2), this.transform.position.z);

                if ((gameObject.transform.localScale.y) >= growthFloat - 0.2f)
                {
                    fullGrown = true;
                }

            }

            if (x && !fullGrown)
            {

                // Set the x position to loop between 0 and 3
                transform.localScale = new Vector3(Mathf.PingPong(Time.time, growthFloat), this.transform.localScale.y, this.transform.localScale.z);
                transform.position = new Vector3((Mathf.PingPong(Time.time, growthFloat) / 2), this.transform.position.y, this.transform.position.z);

                if ((gameObject.transform.localScale.x) >= growthFloat - 0.2f)
                {
                    fullGrown = true;
                }
            }

            if (z && !fullGrown)
            {

                // Set the x position to loop between 0 and 3
                transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, Mathf.PingPong(Time.time, growthFloat));
                transform.position = new Vector3(this.transform.position.x, this.transform.position.y, (Mathf.PingPong(Time.time, growthFloat) / 2));


                if ((gameObject.transform.localScale.z) >= growthFloat - 0.2f)
                {
                    fullGrown = true;
                }
            }
        }
        else
        {
            delayTimer = delayTimer - Time.deltaTime;
        }

    }
}
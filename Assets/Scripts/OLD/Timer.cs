﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float time;
    public float currentTime;


    // Start is called before the first frame update
    void Start()
    {
       // resetTimer = time;
        currentTime = time;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime = currentTime - Time.deltaTime;
        if (currentTime <= 0)
        {
            Debug.Log("Timer Activated and Reset");
            currentTime = time;
        }
    }
}

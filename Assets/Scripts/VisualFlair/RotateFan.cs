﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFan : MonoBehaviour
{
	public bool startSpinning;
	public ParticleSystem PS;
	public GameObject destoyObj;
	public float degrees = 50;
	void Update()
	{
		if (startSpinning)
		{
			transform.Rotate(0, 0, degrees * Time.deltaTime);
		}
	}

	public void ActivateSpin(float time)
	{
		StartCoroutine(TimeSpin(time));
	}

	public IEnumerator TimeSpin(float t)
	{
		startSpinning = true;
		yield return new WaitForSeconds(t);
		startSpinning = false;
		PS.Play();
		Destroy(destoyObj);
    Destroy(destoyObj, 3);

	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Centrifuge : MonoBehaviour
{
    private List<Element> _elements = new List<Element>();

    private Element _element1;
    private Element _element2;
    private Element _element3;
    private bool _isMixing;

    public float mixTime = 2.0f;

    public Element solutionElement1;
    public Element solutionElement2;
    public Element solutionElement3;

  public RotateFan RF;

    public void FillElements(Element e)
    {
        _elements.Add(e);
    Debug.Log("element added");
    }

    public void RemoveElements(Element e)
    {
        _elements.Remove(e);
    Debug.Log("element removed");
  }

    //ELEMENT 1
    public void SetElement1(Element e)
    {
        _element1 = e;
    }

    public Element GetElement1()
    {
        return _element1;
    }

    //ELEMENT 2
    public void SetElement2(Element e)
    {
        _element1 = e;
    }

    public Element GetElement2()
    {
        return _element2;
    }

    //ELEMENT 3
    public void SetElement3(Element e)
    {
        _element3 = e;
    }

    public Element GetElement3()
    {
        return _element3;
    }

    public bool GetIsMixing()
    {
        return _isMixing;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            FillElements(solutionElement1);

        if (Input.GetKeyDown(KeyCode.W))
            FillElements(solutionElement2);

        if (Input.GetKeyDown(KeyCode.E))
            FillElements(solutionElement3);

        if (Input.GetKeyDown(KeyCode.F))
            ActivateCentrifuge();

        if (Input.GetKeyDown(KeyCode.X)) { 
            _elements.Clear();
            _element1 = null;
            _element2 = null;
            _element3 = null;
        }
    }

    public void ActivateCentrifuge()
    {
        StartCoroutine(Mix(mixTime));
    
    }

    IEnumerator Mix(float time)
    {
        float i = 0;
        float rate = 1 / time;
        _isMixing = true;

        while (i < 1)
        {
            i += Time.deltaTime * rate;
            //print("Mixing " + Time.time);E
            yield return 0;
        }

        _isMixing = false;

        if (_elements.Count == 3)//normally 3
        {
        Debug.Log("enough elements are in the mix");
            _element1 = _elements[0];
            _element2 = _elements[1];
            _element3 = _elements[2];
    }

        if ((_element1 == solutionElement1 || _element1 == solutionElement2 || _element1 == solutionElement3) &&
            (_element2 == solutionElement1 || _element2 == solutionElement2 || _element1 == solutionElement3) &&
            (_element3 == solutionElement1 || _element3 == solutionElement2 || _element3 == solutionElement3))
            
        {
            print("Done Mixing: Player's a fucking genius.");
            SolutionMix.correct = true;
            RF.StartCoroutine(RF.TimeSpin(mixTime));
    }
        else
        {
            print("Done Mixing: Not the Solution.");
            SolutionMix.correct = false;
            //play wrong noise
        }
    }

    public void FillElements(GameObject obj)
    {
        _elements.Add(obj.GetComponentInChildren<Element>());
    Debug.Log("element added");
    ActivateCentrifuge();
  }

    public void RemoveElement(GameObject obj)
    {
      _elements.Remove(obj.GetComponentInChildren<Element>());
    Debug.Log("element removed");
    ActivateCentrifuge();

  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainmentCompound : MonoBehaviour
{
    public static bool isDissolved = false;

    public void testDissolve()
    {
        if (SolutionMix.correct)
        {
            //play dissolve animation
            isDissolved = true;
        }
        else
        {
            //show the player it failed
        }
    }
}

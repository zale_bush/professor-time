﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MiniGame : MonoBehaviour
{
  //  private static Random rng = new Random();

    public GameObject[] spawnPoints;
    public List<Vector3> points;
    public List<bool> usedSpawedPoint;
    public List<bool> gameObjectsSpawed;

    private void Awake()
    {
        points.Clear();
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject goPoints  in spawnPoints)
        {
            points.Add(goPoints.transform.position);
            usedSpawedPoint.Add(false);
            gameObjectsSpawed.Add(false);
        }
        ScrambleOrCheeseIT();
    }

    void OnGUI()
    {
        if (GUILayout.Button("Reset"))
        {
            ResetGame();
        }
    }

    public void ResetGame()
    {
        for (int i = 0; i < points.Count; i++)
        {
            usedSpawedPoint[i] = false;
            gameObjectsSpawed[i] = false;
        }

        ScrambleOrCheeseIT();
    }

    public void ScrambleOrCheeseIT()
    {
        int number = points.Count;

        while (number > 0)
        {
            int randoNumber = Random.Range(0, points.Count);
            if (!usedSpawedPoint[randoNumber])
            {
                int rando2 = Random.Range(0,points.Count);
                if (!gameObjectsSpawed[rando2])
                {
                    spawnPoints[randoNumber].transform.position = points[rando2];
                    usedSpawedPoint[randoNumber] = true;
                    gameObjectsSpawed[rando2] = true;
                    number--;
                }
            }
        }
    }

}

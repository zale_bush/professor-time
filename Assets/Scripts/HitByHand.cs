﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitByHand : MonoBehaviour
{
  public UnityEvent pressed;
  public UnityEvent unpressed;

  GameObject hand;

  private void OnTriggerEnter(Collider other)
  {
    if (other.tag == "Player")
    {
      hand = other.gameObject;
      pressed.Invoke();
    }
  }
  private void OnTriggerExit(Collider other)
  {
    if(other.gameObject == hand)
    {
      hand = null;
      unpressed.Invoke();
    }
  }
}

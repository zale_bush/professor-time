﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLaser : MonoBehaviour
{
    public float moveForce = 0f;
    private Rigidbody rbody;
    public GameObject laser;
    public Transform emitter;
    public float fireRate = 0f;
    public float shootForce = 1f;
    private float shootRateTimestamp;
    public void ActivateLaser()
    {
        Instantiate(laser, emitter.position, emitter.rotation);
    }
    // Start is called before the first frame update
    void Start()
    {
        rbody = GetComponent<Rigidbody>();
        //ActivateLaser();
    }

    // Update is called once per frame
    void Update()
    {
        float x = emitter.rotation.x;
        float y = emitter.rotation.y;
        rbody.velocity = new Vector3(x, y, 0);

        if(Input.GetKey(KeyCode.Space))
        {
            if(Time.time > shootRateTimestamp)
            {
                GameObject go = (GameObject)Instantiate(laser, emitter.position, emitter.rotation);
                go.GetComponent<Rigidbody>().AddForce(emitter.forward * shootForce);
                shootRateTimestamp = Time.time + fireRate;
            }
        }
    }
}

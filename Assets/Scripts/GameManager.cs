﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  [System.Serializable]
  public enum LevelType { Intro, ChemLab, LaserMaze, Potato, SoundTracking, Ending }
  [System.Serializable]
  public enum ProgressSates { Unreached, Currently, Completed }


  public GameObject PlayerRig;
  public GameObject ambientPos;

  public level[] levels = new level[6];

  [System.Serializable]
  public class level
  {
    public string name;
    public LevelType state = LevelType.Intro;
    public float clueRevealTime = 60;
    public string[] clues;

    public Transform respawnPosition;


    ProgressSates progress = ProgressSates.Unreached;
    float timer;
    int clueIndex;


    void TimerUpdateTimer(float deltaTime)
    {
      timer += deltaTime;
      if (timer >= clueRevealTime)
      {
        if (clueIndex < clues.Length - 1)
          GlobalAudioManager.instance.PlayAudio(clues[clueIndex], GlobalAudioManager.soundTypes.dialoug);
      }
    }
  }

  [HideInInspector]
  public static GameManager instance;


  int currentLevelIndex;
  

  private void Start()
  {
    if (GameManager.instance == null)
      GameManager.instance = this;
    else
      Destroy(gameObject);
  }

  void NextState()
  {
    print(levels[currentLevelIndex].state.ToString());
    currentLevelIndex++;
    print(levels[currentLevelIndex].state.ToString());
  }

  void NextStateEffects()
  {
    if (currentLevelIndex == 0)
    {
      ToIntro();
    }
    if (currentLevelIndex == 1)
    {
      ToChemLab();
    }
    if (currentLevelIndex == 2)
    {
      ToLazerMaze();
    }
    if (currentLevelIndex == 3)
    {
      ToPotato();
    }
    if (currentLevelIndex == 4)
    {
      ToSoundTracking();
    }
    if (currentLevelIndex == 5)
    {
      ToEnding();
    }
  }

  void ToIntro()
  {

  }

  void ToChemLab()
  {

  }

  void ToLazerMaze()
  {

  }

  void ToPotato()
  {

  }

  void ToSoundTracking()
  {

  }

  void ToEnding()
  {

  }

  public void Die()
  {
    RewindBLink();
    //particle
    PlayerRig.transform.position = levels[currentLevelIndex].respawnPosition.position;
  }

  void RewindBLink()
  {
    //vrtk camera blink
    //play rewind audio
  }
}

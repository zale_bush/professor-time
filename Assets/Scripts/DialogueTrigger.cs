﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{

  #region Attributes

  bool audioplayed = false;
  public string dialougIdentifier;

  #endregion

  private void OnTriggerEnter(Collider other)
  {
    if (other.tag == "PlayerObject" && !audioplayed)
    {
      print(dialougIdentifier);
      //GlobalAudioManager.instance.PlayAudio(dialougIdentifier, GlobalAudioManager.soundTypes.dialoug);
      Destroy(gameObject);
    }
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBolt : MonoBehaviour
{
    public GameObject bolt;
    public GameObject emitter;
    public GameObject player;
    private float distance;
    private float chargeCounter = 0;
    private ParticleSystem ps;
    private float transitionSpeed = 0.13f;
    private Color startColor = Color.green;
    private Color endColor = Color.red;
    private float startTime = Time.time;
    private bool lostPlayer;

    private void chargeLaser()
    {

        float t = (Time.time - startTime) * transitionSpeed;
        chargeCounter = chargeCounter + Time.deltaTime;
        ps.startColor = Color.Lerp(startColor, endColor, t);
        if (ps.isStopped)
        {
            ps.Play();
        }
        if (chargeCounter >= 9)
        {
            fireTurret();
            chargeCounter = 0;
            ps.Stop();
        }
    }

    private void fireTurret()
    {
        GameObject projectile = Instantiate(bolt, emitter.transform.position, emitter.transform.rotation);
        projectile.transform.position = emitter.transform.position;
        Rigidbody rb = projectile.GetComponent<Rigidbody>();
        rb.velocity = emitter.transform.forward * 15;
        ps.startColor = startColor;
        startTime = Time.time;
    }

    public void detectPlayer()
    {
        if (lostPlayer)
        {
            startTime = Time.time;
        }
        lostPlayer = false;
    }
    void Update()
    {
        ps = emitter.GetComponent<ParticleSystem>();
        distance = Vector3.Distance(emitter.transform.position, player.transform.position);
        if (distance < 5)
        {
            detectPlayer();
            chargeLaser();
        }
        else
        {
            lostPlayer = true;
            chargeCounter = 0;
            ps.Stop();
            // Player too far
        }
    }
}
